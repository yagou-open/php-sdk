<?php
/**
 * 雅购商品列表数据获取接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxOrderDetailRequest implements YaGouRequest
{

    private $bizContent = array(
        // 订单Id
        'id'                => null,

        // 分页页码
        'show_page'         => null,
        // 每页加载数据
        'show_limit'        => null,
        // 排序字段
        'order_field'       => null,
        // 排序类型
        'order_type'        => null,
    );

    public function __construct($param=null)
    {
        $this->bizContent = array_merge($this->bizContent,$param);
    }

    public function set($name,$value){
        $this->bizContent[$name] = $value;
    }

    public function get($name){
        return $this->bizContent[$name];
    }

    public function getApiMethodName()
    {
       return "yagou.yggx.order.detail.query";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }
}