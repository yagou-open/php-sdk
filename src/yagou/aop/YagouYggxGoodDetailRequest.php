<?php
/**
 * 雅购商品详情信息接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxGoodDetailRequest implements YaGouRequest
{

    private $bizContent = array(
        // 产业集群ID
        'good_id'     => null,
    );

    public function __construct($param=array())
    {
        $this->bizContent = array_merge($this->bizContent,$param);
    }

    public function set($name,$value){
        $this->bizContent[$name] = $value;
    }

    public function get($name){
        return $this->bizContent[$name];
    }

    public function getApiMethodName()
    {
       return "yagou.yggx.good.detail.query";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }
}