<?php
/**
 *
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 09/03/2018
 * Time: 11:05
 */

namespace yagou\aop;

/**
 * 雅购统一请求入口
 * Interface YaGouRequest
 * @package yagou\aop
 */
interface YaGouRequest
{

    public function getApiMethodName();

    public function getApiVersion();

    public function getApiParas();
}