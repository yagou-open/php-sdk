<?php
/**
 * 雅购商品列表数据获取接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxGoodPullRequest implements YaGouRequest
{

    private $bizContent = array(
        // 产业集群ID
        'cluster_id'     => null,
        // 分类ID
        'category_id'    => null,
        // 商家ID
        'seller_uid'     => null,
        // 店铺ID
        'shop_id'        => null,
        // 商品名称（ 只需要传入一个关键词即可 ）
        'name'           => null,


        // 分页页码
        'show_page'         => null,
        // 每页加载数据
        'show_limit'        => null,
        // 排序字段
        'order_field'       => null,
        // 排序类型
        'order_type'        => null,
    );

    public function __construct($param=null)
    {
        $this->bizContent = array_merge($this->bizContent,$param);
    }

    public function set($name,$value){
        $this->bizContent[$name] = $value;
    }

    public function get($name){
        return $this->bizContent[$name];
    }

    public function getApiMethodName()
    {
       return "yagou.yggx.good.pull.query";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }
}