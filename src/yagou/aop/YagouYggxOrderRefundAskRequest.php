<?php
/**
 * 雅购订单申请退款接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxOrderRefundAskRequest implements YaGouRequest
{

    private $bizContent = array(
        'orderId'           => null,

        // 退款类型,类型：RETURN_PRODUCT, 退货退款， ONLY_REFUND:仅退款
        'refundType'        => null,
        // 退货数量
        'returnData'        => array(),
        // 退款原因 如 多拍/拍错/不想要,其他,退运费,商品瑕疵,质量问题,与描述不符,少件/漏发,与描述不符,收到商品时有划痕或破损,未按约定时间发货
        'cause'             => null,
        // 消费者备注信息，如 尺码太小了，穿着不舒服
        'remark'            => null,
        // 凭证图片地址，可以远程访问的图片url地址
        'imgs'              => array(),
    );

    /**
     * 退货退款
     * @param $orderId 订单Id
     * @return YagouYggxOrderRefundAskRequest
     */
    static function refundProduct($orderId){
        $ask = new self('RETURN_PRODUCT',$orderId);

        return $ask ;
    }

    /**
     * 仅退款
     * @param $orderId 订单Id
     * @return YagouYggxOrderRefundAskRequest
     */
    static function onlyRefund($orderId){
        $ask = new self('ONLY_REFUND',$orderId);

        return $ask ;
    }

    /**
     * YagouYggxOrderRefundAskRequest constructor.
     * @param $refundType 退款类型
     * @param $orderId 订单的id
     */
    private function __construct($refundType,$orderId)
    {
        $this->bizContent['refundType'] = $refundType;
        $this->bizContent['orderId'] = $orderId;
    }

    /**
     *
     * 设置退货退款的数量
     * TODO 如果是退整订单，无需调用这个指定退款的接口
     * @param $orderProductId 订单详情Id
     * @param $returnAmount 退款数量（退款数量不能超过购买数量）
     * @param int $refundMoney 退款金额
     */
    public function setReturnAmount($orderProductId,$returnAmount,$refundMoney=0){

        $this->bizContent['returnData'][$orderProductId] = array(
            'orderProductId' => $orderProductId,
            'returnAmount' => $returnAmount,
            'refundMoney' => $refundMoney,
        );
    }

    /**
     * 设置详情
     * @param $imgs
     */
    public function setImgs($imgs){
        if (is_array($imgs)){
            $this->bizContent['imgs'] = implode(',',$imgs);
        }else{
            $this->bizContent['imgs'] = $imgs;
        }
    }

    /**
     * 原因
     * @param $cause
     */
    public function setCause($cause){
        $this->bizContent['cause'] = $cause;
    }

    /**
     * 备注信息
     * @param $remark
     */
    public function setRemark($remark){
        $this->bizContent['remark'] = $remark;
    }

    public function getApiMethodName()
    {
       return "yagou.yggx.order.refund.ask";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        $this->bizContent['returnData'] = array_values($this->bizContent['returnData']);
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }

}