<?php
/**
 * 雅购订单申请退款接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxOrderRefundEditRequest implements YaGouRequest
{

    private $bizContent = array(
        'refundId'          => null,
        // 退货数量
        'returnAmount'      => null,
        // 退款原因 如 多拍/拍错/不想要,其他,退运费,商品瑕疵,质量问题,与描述不符,少件/漏发,与描述不符,收到商品时有划痕或破损,未按约定时间发货
        'cause'             => null,
        // 消费者备注信息，如 尺码太小了，穿着不舒服
        'remark'            => null,
        // 凭证图片地址，可以远程访问的图片url地址
        'imgs'              => array(),
    );

    /**
     * 编辑的退款id
     * YagouYggxOrderRefundAskRequest constructor.
     * @param $refundId
     */
    public function __construct($refundId)
    {
        $this->bizContent['refundId'] = $refundId;
    }

    /**
     * 退货数量，仅退货退款的时候用到
     * @param $returnAmount
     */
    public function setReturnAmount($returnAmount){

        $this->bizContent['returnAmount'] =  $returnAmount;
    }

    /**
     * 设置详情
     * @param $imgs
     */
    public function setImgs($imgs){
        if (is_array($imgs)){
            $this->bizContent['imgs'] = implode(',',$imgs);
        }else{
            $this->bizContent['imgs'] = $imgs;
        }
    }

    /**
     * 原因
     * @param $cause
     */
    public function setCause($cause){
        $this->bizContent['cause'] = $cause;
    }

    /**
     * 备注信息
     * @param $remark
     */
    public function setRemark($remark){
        $this->bizContent['remark'] = $remark;
    }

    public function getApiMethodName()
    {
        return "yagou.yggx.order.refund.edit";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }

}