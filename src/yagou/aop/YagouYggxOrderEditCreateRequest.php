<?php
/**
 * 雅购商品列表数据获取接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxOrderEditCreateRequest implements YaGouRequest
{

    private $bizContent = array(
        // 收货人姓名
        'full_name'             => null,
        // 收货人手机号
        'mobile_num'            => null,
        // 收货人详细地址,精确到街道门牌号
        'address_detail'        => null,
        // 系统内置地址，如：浙江省,杭州市,江干区,下沙街道
        'address_system'        => null,
        // 系统内置地址，如：33,3301,330104,330104010
        'address_system_code'   => null,
        // 第三方合作伙伴交易号,同一个合作伙伴下不能同时存在相同的交易号
        'partner_num'           => null,
        // 购买规则,['product_id'=>'产品Id','sku_id'=>'sku_id','buy_amount'=>'购买数量','freight_template_id'=>'运费模板Id(如果产品包邮可以不传)']
        'buy_rule'              => array(),
    );

    public function __construct($partnerNum)
    {
        $this->bizContent['partner_num'] = $partnerNum;
    }

    /**
     * 设置收货地址
     * @param $full_name        收货人姓名
     * @param $mobile_num       手机号
     * @param $address_detail   详细收货地址
     * @param $address_system   系统组内置地址，(省,市,县,乡)
     * @param $address_system_code 系统组内置地址编码，(省,市,县,乡)
     */
    public function setAddress($full_name,$mobile_num,$address_detail,$address_system,$address_system_code){
        $this->bizContent['full_name'] = $full_name;
        $this->bizContent['mobile_num'] = $mobile_num;
        $this->bizContent['address_detail'] = $address_detail;
        $this->bizContent['address_system'] = $address_system;
        $this->bizContent['address_system_code'] = $address_system_code;
    }

    /**
     * 添加购买规则，可多次添加
     * @param $product_id   购买产品Id
     * @param $sku_id       规则Id
     * @param $buy_amount   购买数量
     * @param null $freight_template_id 运费模板Id
     * @param string $remark 备注信息
     * @return string
     */
    public function addBuyRule($product_id,$sku_id,$buy_amount,$freight_template_id=null,$remark=''){
        $key = $product_id.'_'.$sku_id;
        $this->bizContent['buy_rule'][$key] = array(
            'product_id'    => $product_id,
            'sku_id'        => $sku_id,
            'buy_amount'    => $buy_amount,
            'freight_template_id'   => $freight_template_id,
            'remark'        => $remark,
        );
        return $key;
    }

    /**
     * 删除购买规则
     * @param $key
     */
    public function delBuyRule($key){
        unset($this->bizContent['buy_rule'][$key]);
    }

    public function getApiMethodName()
    {
       return "yagou.yggx.order.edit.create";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        $this->checkParas();
        $this->bizContent['buy_rule'] = array_values($this->bizContent['buy_rule']);
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }

    /**
     * 检查参数是否正确
     */
    protected function checkParas(){
        $bizContent = $this->bizContent;
        if($bizContent['full_name'] == null || trim($bizContent['full_name']) == ''){
            throw new \Exception("收货人姓名不能为空");
        }
        if($bizContent['mobile_num'] == null || trim($bizContent['mobile_num']) == ''){
            throw new \Exception("收货人联系号码不能为空");
        }else if(count(trim($bizContent['mobile_num'])) >= 12){
            throw new \Exception("收货人联系号码不能超过11位");
        }
        if($bizContent['address_detail'] == null || trim($bizContent['address_detail']) == ''){
            throw new \Exception("收货地址不能为空");
        }
        if($bizContent['address_system'] == null || trim($bizContent['address_system']) == ''){
            throw new \Exception("收货地址(系统内置地址)不能为空");
        }else if(count(explode(',',$bizContent['address_system'])) < 3){
            throw new \Exception("收货地址(系统内置地址)至少传入省,市,县 3级地地址");
        }
        if($bizContent['address_system_code'] == null || trim($bizContent['address_system_code']) == ''){
            throw new \Exception("收货地址(系统内置地址编码)不能为空");
        }else if(count(explode(',',$bizContent['address_system_code'])) < 3){
            throw new \Exception("收货地址(系统内置地址编码)至少传入省,市,县 3级地地址");
        }

        // 检查购买规则是否正确
        foreach ($bizContent['buy_rule'] as $rule){
            if($rule['product_id'] == null || $rule['product_id'] <= 0){
                throw new \Exception("购买规则产品Id不能为空");
            }
            if($rule['sku_id'] == null || $rule['sku_id'] <= 0){
                throw new \Exception("购买规则Id不能为空");
            }
            if($rule['buy_amount'] == null || $rule['buy_amount'] <= 0){
                throw new \Exception("购买数量不能为空");
            }
        }
    }
}