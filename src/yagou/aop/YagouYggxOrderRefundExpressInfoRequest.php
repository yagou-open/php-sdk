<?php
/**
 * 雅购订单申请退款接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxOrderRefundExpressInfoRequest implements YaGouRequest
{

    private $bizContent = array(
        'refundId'          => null,
        // 快递单号
        'expressNum'        => null,
        // 物流备注信息
        'remark'            => null,
        // 物流单号凭证地址，可以远程访问的图片url地址
        'imgs'              => array(),
    );

    /**
     * 编辑的退款id
     * YagouYggxOrderRefundAskRequest constructor.
     * @param $refundId
     */
    public function __construct($refundId)
    {
        $this->bizContent['refundId'] = $refundId;
    }

    /**
     * 快递单号
     * @param $expressNum
     */
    public function setExpressNum($expressNum){

        $this->bizContent['expressNum'] =  $expressNum;
    }

    /**
     * 设置详情
     * @param $imgs
     */
    public function setImgs($imgs){
        if (is_array($imgs)){
            $this->bizContent['imgs'] = implode(',',$imgs);
        }else{
            $this->bizContent['imgs'] = $imgs;
        }
    }

    /**
     * 备注信息
     * @param $remark
     */
    public function setRemark($remark){
        $this->bizContent['remark'] = $remark;
    }

    public function getApiMethodName()
    {
        return "yagou.yggx.order.refund.express.info";
    }
    
    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }

}