<?php
/**
 * 雅购订单申请退款接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace yagou\aop;

class YagouYggxOrderRefundCancelAskRequest implements YaGouRequest
{

    private $bizContent = array(
        'refundId'           => null,
    );
    
    /**
     * 编辑的退款id
     * YagouYggxOrderRefundAskRequest constructor.
     * @param $refundId
     */
    public function __construct($refundId)
    {
        $this->bizContent['refundId'] = $refundId;
    }

    public function getApiMethodName()
    {
        return "yagou.yggx.order.refund.cancel.ask";
    }

    public function getApiVersion()
    {
        return "1.0";
    }

    public function getApiParas()
    {
        return json_encode($this->bizContent,JSON_UNESCAPED_UNICODE);
    }

}