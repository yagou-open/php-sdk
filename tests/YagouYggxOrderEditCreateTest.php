<?php
/**
 * 雅购商品列表数据获取接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace tests;

use PHPUnit\Framework\TestCase;
use yagou\aop\YagouYggxOrderEditCreateRequest;
use yagou\YgClient;

class YagouYggxOrderEditCreateTest extends TestCase
{

    public function atestExecute(){

        $ygClient = new YgClient();

        $ygClient->rsaPrivateKeyPemFilePath = __DIR__.'/config/client_test_privily.pem';
        $ygClient->rsaPrivateKeyPem = 'MIIEogIBAAKCAQEAoDsQa3hBbsbhVUEJ+I1eoRKivtcf4rKFhSduEeH389P098lxKEVzqHeUc0KRC+53dzwOihBfgweKjIUE9Vk10PBxbNHj7ynqqB3MmXP7mK5BZo4SXcTX85jWV9dgXvvJTpjTIoZMJeRT8+rmMRt0YHaxO57nuqkXoH5APwN1eKYZRT/pIL0xzoqpcKxRZt1M2iD64Qnf6LMb0mswcJfyTgwaNQIaJ2PrnPQAI3/48q9IP7cFV3ugdbIuVs2z641AcZLSR9kyMEd569HiFKqZJW741WVPcajwoiH0GHnMVtYv3kb52/ubN1X8v88tNOA16dej86fUuP80b149GnUQQIDAQABAoIBAAUNF51rsxRc7KmVXU0al8sYHU1K1tR8sDzcW1nw97IWEnjlPs34MoczDdj8Vjo8EEQP/p1Xmss0p6d6YgeiAlJN4iV+EGNvvNgD6HlF8L6Fb8Zd4DJYjD4/qTIntgAIi/YQV0eVWD0xj3v3Ab62kNqho0Cv57o4rmf+0uOppkBhX1F7D2dOPqxyOs92uLq/MOPdWmwMYBxGxennRTnaA01svZ2Q6kdtOyrXWf8UVb6zV+bspgINXdwktxbZiTEF1c/sA7O1wAh/YdGnDn/WnujeLQ7DVvmPm/X7a816wLyGGwUqF2utjOE4Aiq575GTDfi9/M3R7alrY7Z62W4IiekCgYEAzCCHPmAw6sDnTzcFGlV9WGLQRto1tekpE2slS7kIJh5e5UiAVWiPFLnTz4qxGfv4c4NGnlKV+T5pisatlNIlra2BTxO8qH4chBoRQNgfwr0mpDGnJGiEvCjc4vgUeS+m8vXPccUDn3Kj7DWWbIDCrJYH7W3RtNwfwfHsP58bRbMCgYEAyPLcvUsfu/CH77Mk1jxCNc0YU7GMYSaZLCtmKOMnH7c8C6VXb4zhTZhe9/smZiYjQCpqD9O14ME/vOL87kAR91/wRtZKO6Qt2tol0E3sLtef7DV5MQf9EcfXiPovaqB0RKuJ3vL4As0CunTI7n+S/eplAx+qtAYgCBZEuFtCLDsCgYB5icCc3g8iJ1tSKhNKu64XKzaRiSlDt6DyY/Bf5rq4X4rm/8URlWDj2UUJJK4QJ5ulw3sPl2KedY/HxkXh/0HZ6B2+5KSGuhfme9LcBhLbE15nkvzOy8CEe9wQ897UzQ+dR8iNF93xWQPLB5kZ34yJ0AbGUoXzJsFt68pbkzNcywKBgBCRDSfB+MTKyLo3P7M11pOO7BGCUrtzri8ZCNwYFhKkTZv8LqAwsiZuiG3TU9AaFs/kb0gMwTLdW77by9Rsjc6iJzH/2rDeETW4xGNO+UjHWBTmN66WJQQBQMC3N4+H2O4MYlpyF4ZLloz+sc4P0HOEjn1ab/1Vf/CKvXGhfGwfAoGAEw06FBl55CNrqH5ScMrsPLQIwQ6qtOM4Tz9t9PupwV3SAHIeocOOwOdH98x9aFyn+z8ccwe8POrQJuzTl9O54qh+Wq9A9UBKK68e5G0yYBxHZzrFOJ+inGQGlAfWurU/5e2gJ2s2IEfs7YL/prjI1xE2hNj6p9n5NY+lAK/r7Pg=';
        $ygClient->yagouPublicKeyPemPath = __DIR__.'/config/yagou_public.pem';
        $ygClient->gatewayUrl = 'http://openapi.dev.php.yggx.com:8008/gateway';
        $ygClient->appId = "YG201803141035";
        $ygClient->debugInfo = true;

        $full_name			= '张三';
        $mobile_num			= '13111111111';
        $address_detail		= '六号大街3幢3楼';
        $address_system		= '浙江省,杭州市,江干区,下沙街道';
        $address_system_code	= '33,3301,330104,330104010';

        $yagouYggxOrderEditCreate = new YagouYggxOrderEditCreateRequest();

        $yagouYggxOrderEditCreate->setAddress($full_name,$mobile_num,$address_detail,$address_system,$address_system_code);
        $yagouYggxOrderEditCreate->addBuyRule(13530,82220,8,1919);
        $param =  $ygClient->execute($yagouYggxOrderEditCreate);
        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";

    }

}