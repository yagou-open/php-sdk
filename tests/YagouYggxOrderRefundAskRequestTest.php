<?php
/**
 * 雅购商品列表数据获取接口
 * Created by PhpStorm.
 * User: wuciyou
 * Email: 898060380@qq.com
 * Date: 08/03/2018
 * Time: 11:02
 */
namespace tests;

use PHPUnit\Framework\TestCase;
use yagou\aop\YagouYggxOrderEditCreateRequest;
use yagou\aop\YagouYggxOrderRefundAskRequest;
use yagou\aop\YagouYggxOrderRefundCancelAskRequest;
use yagou\aop\YagouYggxOrderRefundEditRequest;
use yagou\aop\YagouYggxOrderRefundExpressInfoRequest;
use yagou\YgClient;

class YagouYggxOrderRefundAskRequestTest extends TestCase
{

    public function newClient(){
        $ygClient = new YgClient();

        $ygClient->rsaPrivateKeyPemFilePath = __DIR__.'/config/client_test_privily.pem';
        $ygClient->rsaPrivateKeyPem = 'MIIEogIBAAKCAQEAoDsQa3hBbsbhVUEJ+I1eoRKivtcf4rKFhSduEeH389P098lxKEVzqHeUc0KRC+53dzwOihBfgweKjIUE9Vk10PBxbNHj7ynqqB3MmXP7mK5BZo4SXcTX85jWV9dgXvvJTpjTIoZMJeRT8+rmMRt0YHaxO57nuqkXoH5APwN1eKYZRT/pIL0xzoqpcKxRZt1M2iD64Qnf6LMb0mswcJfyTgwaNQIaJ2PrnPQAI3/48q9IP7cFV3ugdbIuVs2z641AcZLSR9kyMEd569HiFKqZJW741WVPcajwoiH0GHnMVtYv3kb52/ubN1X8v88tNOA16dej86fUuP80b149GnUQQIDAQABAoIBAAUNF51rsxRc7KmVXU0al8sYHU1K1tR8sDzcW1nw97IWEnjlPs34MoczDdj8Vjo8EEQP/p1Xmss0p6d6YgeiAlJN4iV+EGNvvNgD6HlF8L6Fb8Zd4DJYjD4/qTIntgAIi/YQV0eVWD0xj3v3Ab62kNqho0Cv57o4rmf+0uOppkBhX1F7D2dOPqxyOs92uLq/MOPdWmwMYBxGxennRTnaA01svZ2Q6kdtOyrXWf8UVb6zV+bspgINXdwktxbZiTEF1c/sA7O1wAh/YdGnDn/WnujeLQ7DVvmPm/X7a816wLyGGwUqF2utjOE4Aiq575GTDfi9/M3R7alrY7Z62W4IiekCgYEAzCCHPmAw6sDnTzcFGlV9WGLQRto1tekpE2slS7kIJh5e5UiAVWiPFLnTz4qxGfv4c4NGnlKV+T5pisatlNIlra2BTxO8qH4chBoRQNgfwr0mpDGnJGiEvCjc4vgUeS+m8vXPccUDn3Kj7DWWbIDCrJYH7W3RtNwfwfHsP58bRbMCgYEAyPLcvUsfu/CH77Mk1jxCNc0YU7GMYSaZLCtmKOMnH7c8C6VXb4zhTZhe9/smZiYjQCpqD9O14ME/vOL87kAR91/wRtZKO6Qt2tol0E3sLtef7DV5MQf9EcfXiPovaqB0RKuJ3vL4As0CunTI7n+S/eplAx+qtAYgCBZEuFtCLDsCgYB5icCc3g8iJ1tSKhNKu64XKzaRiSlDt6DyY/Bf5rq4X4rm/8URlWDj2UUJJK4QJ5ulw3sPl2KedY/HxkXh/0HZ6B2+5KSGuhfme9LcBhLbE15nkvzOy8CEe9wQ897UzQ+dR8iNF93xWQPLB5kZ34yJ0AbGUoXzJsFt68pbkzNcywKBgBCRDSfB+MTKyLo3P7M11pOO7BGCUrtzri8ZCNwYFhKkTZv8LqAwsiZuiG3TU9AaFs/kb0gMwTLdW77by9Rsjc6iJzH/2rDeETW4xGNO+UjHWBTmN66WJQQBQMC3N4+H2O4MYlpyF4ZLloz+sc4P0HOEjn1ab/1Vf/CKvXGhfGwfAoGAEw06FBl55CNrqH5ScMrsPLQIwQ6qtOM4Tz9t9PupwV3SAHIeocOOwOdH98x9aFyn+z8ccwe8POrQJuzTl9O54qh+Wq9A9UBKK68e5G0yYBxHZzrFOJ+inGQGlAfWurU/5e2gJ2s2IEfs7YL/prjI1xE2hNj6p9n5NY+lAK/r7Pg=';
        $ygClient->yagouPublicKeyPemPath = __DIR__.'/config/yagou_public.pem';
//        $ygClient->gatewayUrl = 'http://openapi.dev.php.yggx.com:8008/gateway';
//        $ygClient->gatewayUrl = 'http://openapi.loc.php.yggx.com/gateway';
        $ygClient->appId = "YG201803141035";
        $ygClient->debugInfo = true;
        return $ygClient;
    }

    /**
     *
     * 取消退款申请
     */
    public function atestCancel(){

        $ygClient = $this->newClient();

        $refundId = 7128;
        $remark = '老板，发顺风快一点快递';


        // 仅退款
        $yagouYggxOrderRefundExpressInfoRequest =  new YagouYggxOrderRefundCancelAskRequest($refundId);

        $param =  $ygClient->execute($yagouYggxOrderRefundExpressInfoRequest);

        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";
    }


    /**
     *
     * 填写退货退款的物流信息
     */
    public function atestRepss(){

        $ygClient = $this->newClient();

        $refundId = 7121;
        $remark = '老板，发顺风快一点快递';


        // 仅退款
        $yagouYggxOrderRefundExpressInfoRequest =  new YagouYggxOrderRefundExpressInfoRequest($refundId);

        $yagouYggxOrderRefundExpressInfoRequest->setRemark($remark);
        $yagouYggxOrderRefundExpressInfoRequest->setExpressNum("wuciyou_快递号");

        $param =  $ygClient->execute($yagouYggxOrderRefundExpressInfoRequest);

        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";
    }

    /**
     *
     * 编辑退款信息
     */
    public function atestedit(){

        $ygClient = $this->newClient();

        $refundId = 7121;
        $remark = '不好意思我买错了';


        // 仅退款
        $yagouYggxOrderRefundEditRequest =  new YagouYggxOrderRefundEditRequest($refundId);

        $yagouYggxOrderRefundEditRequest->setRemark($remark);

        $yagouYggxOrderRefundEditRequest->setImgs(array(
            'http://oss.yggx.com//ueditor2016/227830/2018042410/5ade92f8e79e4.jpg',
        ));

        $param =  $ygClient->execute($yagouYggxOrderRefundEditRequest);

        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";
    }

    /**
     * 仅退款
     * 专家未发货，取消部分商品
     */
    public function testOnlyRefundForSomeGood(){

        $ygClient = $this->newClient();

        $orderId = 136509;
        $cause = '多拍/拍错';
        $remark = '心情不好，不买了';
        $orderProductId = 227817;
        $returnAmount = 1; // 退货数量

        // 图片
        $imgs = array(
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg'
        );

        // 仅退款
        $orderRefundAskRequest = YagouYggxOrderRefundAskRequest::onlyRefund($orderId);

        $orderRefundAskRequest->setCause($cause);
        $orderRefundAskRequest->setRemark($remark);
        $orderRefundAskRequest->setReturnAmount($orderProductId,$returnAmount);
        $orderRefundAskRequest->setImgs($imgs);
        $param =  $ygClient->execute($orderRefundAskRequest);

        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";
    }


    /**
     * 退货退款
     * 退掉整个订单
     */
    public function atestRefundProductForAllTradeGood(){

        $ygClient = $this->newClient();


        $orderId = 132701;
        $cause = '多拍/拍错';
        $remark = '商品质量不行';

        // 图片
        $imgs = array(
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg'
        );
        // 退货退款
        $orderRefundAskRequest = YagouYggxOrderRefundAskRequest::refundProduct($orderId);

        $orderRefundAskRequest->setCause($cause);
        $orderRefundAskRequest->setRemark($remark);
        $orderRefundAskRequest->setImgs($imgs);

        // TODO 退掉整个订单，不需要传具体的订单商品详情Id
//        $orderProductId = 223638;
//        $returnAmount = 1; // 退货数量
//        $orderRefundAskRequest->setReturnAmount($orderProductId,$returnAmount);
        $param =  $ygClient->execute($orderRefundAskRequest);

        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";
    }

    /**
     * 退货退款
     * 退货商品退款
     */
    public function atestRefundProductForSomeTradeGood(){

        $ygClient = $this->newClient();

        $orderId = 132701;
        $cause = '多拍/拍错';
        $remark = '商品快递过程中有问题';
        $orderProductId = 223638;
        $returnAmount = 1; // 退货数量

        // 图片
        $imgs = array(
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg',
            'http://oss.yggx.com//ueditor2016/549/2016071911/578d9faf5d9b1.jpg'
        );


        // 退货退款
        $orderRefundAskRequest = YagouYggxOrderRefundAskRequest::refundProduct($orderId);

        $orderRefundAskRequest->setCause($cause);
        $orderRefundAskRequest->setRemark($remark);
        $orderRefundAskRequest->setReturnAmount($orderProductId,$returnAmount);
        $orderRefundAskRequest->setImgs($imgs);

        $param =  $ygClient->execute($orderRefundAskRequest);

        echo json_encode($param,JSON_UNESCAPED_UNICODE);
        echo "\n";
    }

}